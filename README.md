# Jeu avec symfony

Lucky click il faut cliquer sur lucky le plus vite pendant 10 secondes pour gagner la partie.

## Le déroulé

1. Créer le projet Symfony
2. Créer sa base de donnée
3. Créer les pages utilisateurs
4. Créer l'espace admnistration
5. Créer la page jeu
6. Ajouter bootstrap
7. Ce qui fonctionne
8. Reste a amaliorer

## 1 Créer le projet Symfony

Dans le terminal taper :  
symfony new NomDuProjet --webapp --version=5.4

Et on attend .... si tout se passe bien on lit le message suivant :  
[ok] your project is now ready

On peut passer à la suite

## 2 Créer sa base de donnée

Pour stocker les info en BDD faire un petit schema MCD avec:

Une table Utilisateurs,  
Une table parties,  
Penser aux cardinalités.

Dans symfony aller dans .env changer son database_url en indiquant le nom de la BDD  
le mot de passe, le nom du projet.

Dans le terminal taper :  
php bin/console doctrine:database:create

Grace a cette commande on demande a symfony de creer une base de donnée.  
Pour verifier que la base exsiste on va sur phpmyadmin est la surprise c'est cool la base est créée!!! Mais c'est pas fini il faut remplir les tables ....

Dans le terminal taper :  
php bin/console make:user

Et on crée les tables en répondant aux questions.  
si on regarde le dossier entity et repositories. On retrouve les classes  
correspondant aux tables.

Pour les relations Dans le terminal taper :  
php bin/console make:entity

On rappel sa classe puis la classe que l'on souhaite lier.  
Grâce à relation on affecte la cardinalité.

Il reste a créer le code sql et a executer ce code sql. Grâce a deux commandes.  
La premiere commande va créer un dossier migration avec le code sql.  
La seconde commande va exécuter la migration. A toi de composer ta symfony :

Dans le terminal tapes :  
php bin/console make:migration

php bin/console doctrine:migration:migrate

Direction phpmyadmin et la tip top tes tables et tes relations crées.

## 3 Créer les pages utilisateurs

Pour construire les pages du site dans le terminal tapes :  
php bin/console make:controller

Réponds aux questions, et symfony crée ta page dans le dossier src-controller ainsi que le dossier template.  
Dans src-controller tu trouveras la classe, la function, ainsi que le chemin de route. Et "name" : ... Tres utile pour ta navbarre ....

Le template c'est la ou tout nos petits bout de code se compilent grace aux .twig afin de faire le mega site rien que ca.

On peut passer a la création de l'utilisateur.  
Dans le terminal tapes :  
php bin/console make:registration-form

Réponds aux questions... et dans le dossier controller-registercontroller miracle il y a :  
une route /register  
et le formulaire d'enregistrement est la !

Dans le terminal tapes :  
composer require symfonycats/verify-email-bundle

Direction mailtrap pour un petit serveur smtp.

On peut passer a l'authentification  
Dans le terminal tapes :  
php bin/console make:auth

Tu réponds au questions ...

Dans src-controller ca crée un fichier securitycontroller.  
Dans ce fichier il faut decommenter et ajouter le chemin du homecontroller:  
return new RedirectResponse($this->urlGenerator->generate('app_home'));

Pour le lien en cas de mot de passe oublié

Dans le terminal tapes :  
composer require symfonycasts/reset-password-bundle  
php bin/console make:reset-password

Et ca creer la page /reset-password  
On change le mot de passe, on se rend dans mailsttrap, on verifie en Html, en BDD  
et c'est bon.

## 4 Créer l'espace admnistration

Dans le terminal tapes :

php bin/console make:crud

On veut un Crud sur Utilisateurs  
Dans le controller sa crée AdminUserController

## 5 Creer la page jeu

Dans le terminal tapes :

php bin/console make:crud

On veut un Crud pour les parties  
Dans le controller sa crée PartieController

## 6 Ajouter bootstrap

Recuperes le code link pour le responsive ainsi que celui pour le Javascript.

Recuperes et adapte un menu responsive  
Fais les liens entre les pages et le menu

## 7 Ce qui fonctionne

Sur la BDD  
La connexion a la base de donnée est faite.  
Le mot de passe est haché

Sur l'envoie de mail  
L'utilisatuer peut demander a changer son mot de passe

Sur l'utilisateur  
L'utilisateur peut se crééer, se modifier, se supprimer.  
L'utilisateur peut se connecter et se deconnecter.  
L'utilisatuer peut changer son mot de passe.

Sur le jeu  
Le compteur fonctionne au clic  
Le temps s'écoule sur 10 secondes

## 8 Reste a amaliorer

Sur le jeu  
Pouvoir sauvegarder le score en Bdd,  
Ne plus pouvoir faire de clic après 10 secondes.  
Voir ses meilleurs scores,  
Envoyer les meilleurs score en mail.

---
