const departSecondes = 10;
let temps = departSecondes * 60;

const timerElement = document.getElementById("timer");

setInterval(() => {
  let secondes = parseInt(temps / 3600, 10);
  let secondes = parseInt(temps % 3600, 10);

  secondes = secondes < 10 ? "0" + secondes : secondes;
  secondes = secondes < 10 ? "0" + secondes : secondes;

  timerElement.innerText = `${secondes}:${secondes}`;
  temps = temps <= 0 ? 0 : temps - 1;
}, 1000);
