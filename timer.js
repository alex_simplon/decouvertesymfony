const departMinutes = 1;
let temps = departMinutes * 10;

const timerElement = document.getElementById("timer");

setInterval(() => {
  let minutes = parseInt(temps / 60, 10);
  let secondes = parseInt(temps % 60, 10);

  minutes = minutes < 10 ? "0" + minutes : minutes;
  secondes = secondes < 10 ? "0" + secondes : secondes;

  timerElement.innerText = `${minutes}:${secondes}`;
  temps = temps <= 0 ? 0 : temps - 1;
}, 1000);

const btns = document.querySelectorAll(".btn");
const value = document.querySelector("#counter");

// valeur du compteur initialisée à 0
let count = 0;

btns.forEach((btn) => addListenerForEachButton(btn));

// on ajoute un listener avec le bouton en paramètre
function addListenerForEachButton(btn) {
  console.log("Ajout du listener pour chaque bouton");
  btn.addEventListener("click", () => onButtonClicked(btn));
}

function onButtonClicked(btn) {
  if (btn.classList.contains("increase")) {
    // code à exécuter pour le bouton "increase"
    count++;
    console.log("Clic plus vite que ton Ombre : " + count);
  }

  // on met à jour l'affichage pour le compteur (value) dans le HTML
  value.textContent = count;
}
